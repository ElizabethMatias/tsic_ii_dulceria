"""
URL configuration for dulceria project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf import settings
from django.conf.urls.static import static
from apps.carrito.views import *
from apps.producto.views import *
from apps.usuario.views import *
from apps.venta.views import *

urlpatterns = [
    path('admin/', admin.site.urls),
    path('index/', index, name='index'),
    path('registro/', RegistroView.as_view(), name='registro'),
    path('usuario/editar/', UserEditView.as_view(), name='editar_usuario'),
    path('usuario/consultar', UserDetailView.as_view(), name='consultar_usuario'),
    path('usuarios/eliminar', UserDeleteView.as_view(), name='eliminar_usuario'),
    path('login/', user_login, name='login'),
    path('logout/', user_logout, name='logout'),
    path('vendedor/home/', vendedor_home, name='vendedor_home'),  # Vista del homepage del vendedor
    path('cliente/home/', cliente_home, name='cliente_home'),
    path('productos/agregar/', agregar_producto, name='agregar_producto'),
    path('productos/', listar_productos, name='listar_productos'),
    path('productos/editar/<int:id>/', editar_producto, name='editar_producto'),
    path('productos/eliminar/<int:id>/', eliminar_producto, name='eliminar_producto'),
    path('carrito/agregar/<int:id>/', agregar_carrito, name='agregar_carrito'),
    path('carrito/', ver_carrito, name='ver_carrito'),
    path('carrito/incrementar/<int:id>/', incrementar_cantidad_producto_carrito, name='incrementar_cantidad_producto_carrito'),
    path('carrito/decrementar/<int:id>/', decrementar_cantidad_producto_carrito, name='decrementar_cantidad_producto_carrito'),
    path('carrito/eliminar/<int:id>/', eliminar_producto_carrito, name='eliminar_producto_carrito'),
    path('compra/', realizar_compra, name='realizar_compra'),
    path('compra/exitosa/', compra_exitosa, name='compra_exitosa'),
    path('compras/cliente/', compras_cliente, name='compras_cliente'),
    path('ventas/vendedor/', ventas_vendedor, name='ventas_vendedor'),
]
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
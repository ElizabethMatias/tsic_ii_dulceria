from django.contrib import admin
from .models import TipoUsuario

class TipoUsuarioAdmin(admin.ModelAdmin):
    list_display = ('usuario', 'tipo_usuario')  # Muestra estas columnas en la interfaz de administración
    search_fields = ('usuario__username', 'tipo_usuario')  # Permite buscar por estos campos

# Registrar el modelo TipoUsuario con la clase ModelAdmin
admin.site.register(TipoUsuario, TipoUsuarioAdmin)

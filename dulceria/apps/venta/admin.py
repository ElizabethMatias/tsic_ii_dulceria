from django.contrib import admin


from apps.venta.models import *

# admin.site.register(ItemVenta)
# admin.site.register(Venta)
class ItemVentaAdmin(admin.ModelAdmin):
    list_display = ('producto', 'precio','cantidad')

    search_fields = ('precio',)

    list_filter=('cantidad',)
        
# Register your models here.
admin.site.register(ItemVenta, ItemVentaAdmin)
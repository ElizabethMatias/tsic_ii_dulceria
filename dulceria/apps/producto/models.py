from django.db import models
from django.contrib.auth.models import User

# Modelo para Producto
class Producto(models.Model):
    nombre = models.CharField(max_length=100)
    precio = models.DecimalField(max_digits=6, decimal_places=2)
    descripcion = models.TextField()
    imagen = models.ImageField(upload_to='imagenes_productos/', blank=True, null=True)
    vendedor = models.ForeignKey(User, on_delete=models.CASCADE, related_name='productos')
    stock = models.IntegerField(default=0)  
    def __str__(self):
        return self.nombre
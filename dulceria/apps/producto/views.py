from django.shortcuts import render, redirect
from .forms import ProductoForm
from .models import Producto

def agregar_producto(request):
    if request.method == 'POST':
        print(request.user)
        form = ProductoForm(request.POST, request.FILES)
        if form.is_valid():
            producto = form.save(commit=False)
            producto.vendedor = request.user
            producto.save()
            return redirect('listar_productos')  # Redirige a la vista de listar productos
    else:
        form = ProductoForm()
    return render(request, 'producto/agregar_producto.html', {'form': form})

def listar_productos(request):
    productos = Producto.objects.filter(vendedor=request.user)  # Solo muestra los productos del vendedor actual
    return render(request, 'producto/listar_productos.html', {'productos': productos})

def editar_producto(request, id):
    producto = Producto.objects.get(id=id)
    if request.method == 'POST':
        form = ProductoForm(request.POST, request.FILES, instance=producto)
        if form.is_valid():
            form.save()
            return redirect('listar_productos')
    else:
        form = ProductoForm(instance=producto)
    return render(request, 'producto/editar_producto.html', {'form': form})

def eliminar_producto(request, id):
    producto = Producto.objects.get(id=id)
    if request.method == 'POST':
        producto.delete()
        return redirect('listar_productos')
    return render(request, 'producto/eliminar_producto.html', {'producto': producto})

Crear entorno virtual de python
    virtualenv tsiciienv
    source tsiciienv/bin/activate
 
Instalar librerias de python
    pip install -r requirements.txt

Entrar al proyecto
    cd tsic_ii_dulceria/dulceria/

Migrar modelos a la base de datos
    python manage.py makemigrations usuario
    python manage.py makemigrations producto
    python manage.py makemigrations carrito
    python manage.py makemigrations venta

    python manage.py migrate

levantar proyecto
    python manage.py runserver